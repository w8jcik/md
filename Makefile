default:
	./script.sh
	@diff template template-orig || echo "template has been modified!"

record:
	./script.sh > tmp/out 2> tmp/err
	tail -f tmp/{out,err}
	@diff template template-orig || echo "template has been modified!"

clean-min:
	rm -Rf tmp/opls-aa/steep
	rm -Rf tmp/opls-aa/cg
	rm -Rf tmp/amber94/steep
	rm -Rf tmp/amber94/cg
	rm -Rf tmp/opls-aa
	rm -Rf tmp/amber94

clean:
	make clean-min
	rm -Rf tmp/.trash
	rm tmp/* -f

download:
	rsync -az studbi01@p50.ch.pwr.wroc.pl:/home/studbi/studbi01/md/tmp/ /var/run/media/desktop/scratch/ --itemize-changes

fastest:
	tools/get-fastest.sh
