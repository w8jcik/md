#!/bin/bash
source tools/try-helper.bash
source tools/color-helper.bash

dir=$PWD

n_steps=500000

if [ `hostname` = 'arch' ]; then
  n_steps=100
  cd scratch
else
  cd tmp
fi

# Download the PDB file and extract chain A

pdb_code=3ldj

if [ ! -e polyp.pdb ]; then
  mkdir .trash -p
  
  wget -O - "http://www.rcsb.org/pdb/files/${pdb_code}.pdb.gz" \
    | gunzip -c > "${pdb_code}.pdb"
    
  # creates two files 'all-chains.pdb' and 'chain-a.pdb'
  try vmd "${pdb_code}.pdb" -e ../tools/extract-chain-a.vmd.tk -dispdev text
  
  cp chain-a.pdb polyp.pdb
  mv 3ldj.pdb all-chains.pdb chain-a.pdb .trash
fi

f_types=( "amber94" "opls-aa" )
#~ f_types=( "amber94" )

for f_type in "${f_types[@]}"; do
  if [ ! -e "${f_type}/polyp.pdb" ]; then 
    mkdir "${f_type}" -p
    cp polyp.pdb "${f_type}"
  fi
done

# Choose charges of selected residues

if [ ! -e amber94/polyp.gro ]; then
  cd amber94
  
  printf "2111111111100000yyy" | sed -e "s/.\{1\}/&\n/g" \
    | pdb2gmx -f polyp.pdb -o polyp.gro -p polyp.top -inter -water tip3p
  try sed -i 's/HOH/SOL/g' polyp.gro
  
  cd ..
fi

if [ ! -e opls-aa/polyp.gro ]; then
  cd opls-aa
  
  # ff
  # lys +1 (15, 26, 41, 46), arg +1 (1, 17, 20, 39, 42, 53)
  # glu -1 (31), asp-acid -1 (3, 50), glu-acid -1 (7, 49)
  # all three bonds
  # start terminus ARG1 ion, end terminus GLY56 ion
  
  printf "15\n1\n1\n1\n1\n1\n1\n1\n1\n1\n1
0\n0\n0\n0\n0
y\ny\ny
0\n0\n" \
    | pdb2gmx -f polyp.pdb -o polyp.gro -p polyp.top -inter -water tip3p
  try sed -i 's/HOH/SOL/g' polyp.gro
  
  cd ..
fi

for f_type in "${f_types[@]}"; do
  cd "${f_type}"
  
  # Add water, merge two 'SOL' groups

  if [ ! -e polyp-box-solv.gro ]; then
    mkdir .trash -p
    
    editconf -f polyp.gro -o polyp-box.gro -bt triclinic -d 2 \
      && mv polyp.top polyp-box.top
      
    try genbox -cp polyp-box.gro -cs spc216.gro -p polyp-box.top -o polyp-box-solv.gro \
      && mv polyp-box.top polyp-box-solv.top \
       ; mv \#polyp-box.top.1# polyp-box.top
    
    # 1) polyp-box-solv.gro is a polyp-box.gro with an addition of 12428 water molecules
    #
    # 2) polyp.top file is being backed up and the new one has one more
    #    line of SOL which is unfortunately the second SOL group here
    #
    #    ; Compound        #mols
    #    Protein_chain_A     1
    #    SOL                71
    #    SOL             12428
    
    mv polyp-box-solv.top polyp-box-solv-broken.top
    atoms_number=$(cat polyp-box-solv-broken.top | grep -e "SOL" | grep -o -e "[0-9]*" | paste -sd+ | bc)
    grep polyp-box-solv-broken.top -v -e "SOL" > polyp-box-solv.top
    try printf "SOL %17d\n" ${atoms_number} >> polyp-box-solv.top

    # cleaning
    mv polyp-box-solv-broken.top polyp-box.{gro,top} polyp.gro polyp.pdb .trash
  fi

  # Prepare and run two types of minimization

  m_types=( "cg" "steep" )

  if [[ ! -e steep/minimization.log || ! -e cg/minimization.log ]]; then

    for m_type in "${m_types[@]}"; do
      mkdir ${m_type}
    done

    sed 's/2000/-1/g' ../../template/minimize.mdp > steep/minimize.mdp
    sed 's/2000/-1/g' ../../template/minimize.mdp | sed 's/steep/cg/g' > cg/minimize.mdp
    
    diff ../../template/minimize.mdp steep/minimize.mdp > steep/minimize.mdp.diff
    diff ../../template/minimize.mdp cg/minimize.mdp > cg/minimize.mdp.diff

    for m_type in "${m_types[@]}"; do
      cp polyp-box-solv.{gro,top} posre.itp ${m_type}

      cd ${m_type}
      mkdir .trash -p
      
      #~ # try different parameters to satisfy CG
      
      #~ mv minimize.mdp minimize.mdp.original
      #~ cat minimize.mdp.original | grep -v rvdw > minimize.mdp
      #~ echo "rvdw = 1.3" >> minimize.mdp
      #~ echo "rvdw_switch = 0.8" >> minimize.mdp
      
      try grompp -f minimize.mdp -c polyp-box-solv.gro -p polyp-box-solv.top -o unbalanced.tpr
      
      echo "SOL" | genion -s unbalanced.tpr -o polyp-box-solv-ion.gro -p polyp-box-solv.top -nname CL- -nn 6 -pname NA+ -np 0 || die "failed while adding ions"
      
      if [ -e \#polyp-box-solv.top.1# ]; then
        mv polyp-box-solv.top polyp-box-solv-ion.top
        mv \#polyp-box-solv.top.1# polyp-box-solv.top
      fi
        
      echo "q" | make_ndx -f polyp-box-solv-ion.gro
      
      try cp polyp-box-solv-ion.top polyp-box-solv-ion.top.original
      try sed -i 's/CL-/Cl/g' polyp-box-solv-ion.top
      diff polyp-box-solv-ion.top polyp-box-solv-ion.top.original > polyp-box-solv-ion.top.diff
      
      try grompp -f minimize.mdp -c polyp-box-solv-ion.gro -p polyp-box-solv-ion.top -o minimization.tpr
      try mdrun -v -s minimization.tpr -o polyp-box-solv-ion-minimized.trr -c polyp-box-solv-ion-minimized.gro -e polyp-box-solv-ion-minimized.edr -g minimization.log

      ln -sf polyp-box-solv-ion-minimized.gro polyp.gro
      ln -sf polyp-box-solv-ion.top polyp.top
      
      mv polyp-box-solv.{gro,top} mdout.mdp unbalanced.tpr *.diff *.original .trash

      cd ..
      
    done
    
  fi

  if [[ ! -e steep/nvt/md.log ]]; then
    mkdir steep/nvt -p
    cp "${dir}/template/nvt-md.mdp" "steep/nvt/nvt.mdp"
    sed -i "s/200000/${n_steps}/" "steep/nvt/nvt.mdp"
    
    cd steep
    try cp polyp.{gro,top} posre.itp nvt
    
    cd nvt
    mkdir .trash -p
    
    try grompp -f nvt.mdp -c polyp.gro -p polyp.top -o nvt.tpr -maxwarn 2
    try mdrun -v -s nvt.tpr -c polyp-nvt.gro -e polyp-nvt.edr -o polyp-nvt.trr
    
    cd ../..
  fi


  # NPT simualtions after NVT simulation
  
  p_types=( "default" "step-down" "step-up" "rlist-down" "rlist-up" "rvdw-up" "rvdw-up-up" "modres" )
  #~ p_types=( "default" "rlist-down" "rlist-up" )

  for p_type in "${p_types[@]}"; do
    if [ ! -e "steep/nvt/${p_type}/npt.mdp" ]; then
      mkdir "steep/nvt/${p_type}" -p
      ls ..
      cp "${dir}/template/npt-md.mdp" "steep/nvt/${p_type}/npt.mdp"
    fi
  done

  local_type="step-down"

  if [ ! -e "steep/nvt/${local_type}/md.log" ]; then
    sed -i "s/0.001/0.0005/" "steep/nvt/${local_type}/npt.mdp"
    sed -i "s/200000/$((n_steps*2))/" "steep/nvt/${local_type}/npt.mdp"
  fi

  local_type="step-up"

  if [ ! -e "steep/nvt/${local_type}/md.log" ]; then
    sed -i "s/0.001/0.002/" "steep/nvt/${local_type}/npt.mdp"
    sed -i "s/200000/$((n_steps/2))/" "steep/nvt/${local_type}/npt.mdp"
  fi

  local_type="rlist-down"

  if [ ! -e "steep/nvt/${local_type}/md.log" ]; then
    cp "steep/nvt/${local_type}/npt.mdp" "steep/nvt/${local_type}/npt.mdp.original"
    cat "steep/nvt/${local_type}/npt.mdp.original" | grep -v rlist | grep -v rcoulomb | grep -v rvdw > "steep/nvt/${local_type}/npt.mdp"
    echo "rlist = 0.8" >> "steep/nvt/${local_type}/npt.mdp"
    echo "rcoulomb = 0.8" >> "steep/nvt/${local_type}/npt.mdp"
    echo "rvdw = 1.2" >> "steep/nvt/${local_type}/npt.mdp"
  fi

  local_type="rlist-up"

  if [ ! -e "steep/nvt/${local_type}/md.log" ]; then
    cp "steep/nvt/${local_type}/npt.mdp" "steep/nvt/${local_type}/npt.mdp.original"
    cat "steep/nvt/${local_type}/npt.mdp.original" | grep -v rlist | grep -v rcoulomb | grep -v rvdw > "steep/nvt/${local_type}/npt.mdp"
    echo "rlist = 1.2" >> "steep/nvt/${local_type}/npt.mdp"
    echo "rcoulomb = 1.2" >> "steep/nvt/${local_type}/npt.mdp"
    echo "rvdw = 1.2" >> "steep/nvt/${local_type}/npt.mdp"
  fi
  
  local_type="rvdw-up"

  if [ ! -e "steep/nvt/${local_type}/md.log" ]; then
    cp "steep/nvt/${local_type}/npt.mdp" "steep/nvt/${local_type}/npt.mdp.original"
    cat "steep/nvt/${local_type}/npt.mdp.original" | grep -v rvdw > "steep/nvt/${local_type}/npt.mdp"
    echo "rvdw = 1.2" >> "steep/nvt/${local_type}/npt.mdp"
  fi
  
  local_type="rvdw-up-up"

  if [ ! -e "steep/nvt/${local_type}/md.log" ]; then
    cp "steep/nvt/${local_type}/npt.mdp" "steep/nvt/${local_type}/npt.mdp.original"
    cat "steep/nvt/${local_type}/npt.mdp.original" | grep -v rvdw > "steep/nvt/${local_type}/npt.mdp"
    echo "rvdw = 1.4" >> "steep/nvt/${local_type}/npt.mdp"
  fi
  

  #~ for p_type in "${p_types[@]}"; do

    #~ if [ ! -e "steep/nvt/${p_type}/md.log" ]; then
      #~ cd "steep/nvt/${p_type}"
      
      #~ echo -e "${GREEN}${f_type}/${p_type}${NC}"
      
      #~ cp ../polyp-nvt.gro ../polyp.top ../posre.itp .
      
      #~ try sed -i "s/200000/${n_steps}/" npt.mdp
      #~ try grompp -f npt.mdp -c polyp-nvt.gro -p polyp.top -o npt.tpr -maxwarn 1
      #~ try mdrun -v -s npt.tpr -c polyp-npt.gro -e polyp-npt.edr -o polyp-npt.trr
      
      #~ cd ../../..
    #~ fi
    
  #~ done

  # Show results comparison

  #~ for m_type in "${m_types[@]}"; do
    #~ if [ -e "${m_type}/minimization.log" ]; then
      #~ grep "converged to" "${m_type}/minimization.log" --after-context=3
      #~ echo ""
    #~ fi
  #~ done
  
  #~ mkdir -p /tmp/sum
  #~ touch /tmp/sum/core_time

  for p_type in "${p_types[@]}"; do

    if [ -e "steep/nvt/${p_type}/md.log" ]; then
      cd "steep/nvt/${p_type}"
      
      echo -e "${GREEN}${f_type}${NC} → nvt → ${GREEN}npt-${p_type}${NC} measures"
      
      if [ ! -e index.ndx ]; then
        ln -s ../../index.ndx . -f
      fi
      
      mkdir measures -p

      tail md.log | grep -oe "Time:\s*[0-9\.]*" | grep -oe '[0-9\.]*' | xargs printf 'Core time %.3f\n'
      
      # Select protein-H twice
      
      if [ ! -e measures/polyp-rmsd_xvg ]; then
        printf "2\n2\n" | g_rms -s ../polyp-nvt.gro -f polyp-npt.trr -n index.ndx -o measures/polyp_rmsd.xvg
      fi
      
      if [ ! -e measures/rmsd_vs_initial ]; then
        printf "2\n2\n" | g_confrms -f1 ../../../polyp.gro -f2 polyp-npt.gro -o measures/polyp-confrms.pdb > measures/rmsd_vs_initial
      fi

      if [ ! -e measures/polyp-rmsf.xvg ]; then
        printf "4\n" | g_rmsf -s polyp-npt.gro -f polyp-npt.trr -n index.ndx -o measures/polyp-rmsf.xvg
      fi
      
      if [ ! -e measures/polyp-rmsf.xvg ]; then
        printf "2\n" | g_rmsf -res -s polyp-npt.gro -f polyp-npt.trr -n index.ndx -o measures/res-rmsf.xvg
      fi
      
      #~ printf "10\n11\n12\n13\n\n" | g_energy -f polyp-npt.edr -s polyp-npt.gro -o polyp-npt-energy.xvg
      
      #~ g_rama -f polyp-npt.trr -s npt.tpr -o polyp-npt-rama.xvg
      #~ printf "1\n1\n" | g_hbond -f polyp-npt.trr -s npt.tpr -n ../../index.ndx -g polyp-hbond.log -num polyp-npt-hbond-pp.xvg
      #~ printf "1\n13\n" | g_hbond -f polyp-npt.trr -s npt.tpr -n ../../index.ndx -g polyp-hbond.log -num polyp-npt-hbond-ps.xvg
      
      if [ ! -e measures/gyrate.custom.xvg ]; then
        printf "2\n" | g_gyrate -f polyp-npt.trr -s polyp-npt.gro -n index.ndx -o measures/gyrate.xvg
        sed 's/@ view .*$//g' measures/gyrate.xvg | sed "s/\"Radius of gyration.*\"/\"Radius of gyration (${f_type})\"/" > measures/gyrate.custom.xvg
      fi
      
      if [ ! -e measures/polyp-density.xvg ]; then
        printf "1\n" | g_density -f polyp-npt.trr -n index.ndx -s npt.tpr -o measures/polyp-density.xvg
      fi
      
      if [ ! -e measures/sol-density.xvg ]; then
        printf "17\n" | g_density -f polyp-npt.trr -n index.ndx -s npt.tpr -o measures/sol-density.xvg
      fi
      
      cd ../../..
    fi
  done
  
  #~ f="steep/nvt/rlist-down/measures/gyrate.custom.xvg"
  
  #~ sed -i "s/@ s0 legend ".*"$/@ s0 legend \"${f_type}, rlist = 1.2\"/" $f
  #~ sed -i "s/@ s1 legend ".*"$/@ s1 legend \"${f_type}, rlist = 1.0\"/" $f
  #~ sed -i "s/@ s2 legend ".*"$/@ s2 legend \"${f_type}, rlist = 0.8\"/" $f
  
  
  #~ f="steep/nvt/step-up/measures/gyrate.custom.xvg"
  
  #~ sed -i "s/@ s0 legend ".*"$/@ s0 legend \"${f_type}, step = 0.0005\"/" $f
  #~ sed -i "s/@ s1 legend ".*"$/@ s1 legend \"${f_type}, step = 0.001\"/" $f
  #~ sed -i "s/@ s2 legend ".*"$/@ s2 legend \"${f_type}, step = 0.002\"/" $f
  
  
  #~ f="steep/nvt/default/measures/gyrate.custom.xvg"
  
  #~ sed -i "s/@ s0 legend ".*"$/@ s0 legend \"${f_type}, rvdw = 1.4\"/" $f
  #~ sed -i "s/@ s1 legend ".*"$/@ s1 legend \"${f_type}, rvdw = 1.2\"/" $f
  #~ sed -i "s/@ s2 legend ".*"$/@ s2 legend \"${f_type}, rvdw = 1.0\"/" $f


  #~ f="steep/nvt/modres/measures/gyrate.custom.xvg"
  
  #~ if [ -e $f ]; then
    #~ sed -i "s/@ s0 legend ".*"$/@ s0 legend \"${f_type}\"/" $f
    #~ sed -i "s/@ s1 legend ".*"$/@ s1 legend \"${f_type}, eight C-aplha moved by 0.001 nm\"/" $f
  #~ fi
  
  echo -e "\n"

  cd ..
done

#~ f="opls-aa/steep/nvt/default/measures/gyrate.custom.ff_comparison.xvg"

#~ sed 's/@ view .*$//g' "opls-aa/steep/nvt/default/measures/gyrate.xvg" | sed "s/\"Radius of gyration.*\"/\"Radius of gyration\"/" > $f

#~ sed -i "s/@ s0 legend ".*"$/@ s0 legend \"amber94\"/" $f
#~ sed -i "s/@ s1 legend ".*"$/@ s1 legend \"opls-aa\"/" $f


#~ xmgrace amber94/steep/nvt/{rlist-up,rvdw-up,rlist-down}/measures/gyrate.custom.xvg -fixed 600 300
#~ xmgrace amber94/steep/nvt/{step-down,default,step-up}/measures/gyrate.custom.xvg -fixed 600 300
#~ xmgrace amber94/steep/nvt/{rvdw-up-up,rvdw-up,default}/measures/gyrate.custom.xvg -fixed 800 400
#~ xmgrace amber94/steep/nvt/{default,modres}/measures/gyrate.custom.xvg -fixed 800 400
#~ xmgrace amber94/steep/nvt/default/measures/gyrate.custom.xvg opls-aa/steep/nvt/default/measures/gyrate.custom.ff_comparison.xvg -fixed 800 400

cd ..
