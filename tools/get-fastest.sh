#!/bin/bash

# List workstations at p50 with their corresponding loads.
# Should be run at p50 (server).

touch .fastest
if [ `hostname` = "prkom13" ]; then
  for i in {1..13}; do
    ssh prkom${i} uptime 2> err | grep -o -e "load average: .*" | { read load; echo "${load} @ prkom${i}"; } >> .fastest
  done
else
  for i in {0..30}; do
    ssh p50-${i} uptime 2> err | grep -o -e "load average: .*" | { read load; echo "${load} @ p50-${i}"; } >> .fastest
  done
fi
grep "load average" .fastest | sort
rm .fastest
rm err
